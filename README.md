# Using vagrant to create super VM

Vagran automate the VM creation

## Requirements

- Step 1: Download and install **VirtualBox**
- Step 2: Download and install **Vagrant**
- Step 3: Verify that you have openssh in your machine (when you install **git you maybe installed openssh too**)

## Lets get started

- Step 1: write the next command

```
vagrant init
```

This command will create a file `Vagrantfile`

You'll find a file with a template, config like the following piece of code:

```ruby

Vagrant.configure("2") do |config|

  config.vm.box = "ubuntu/focal64"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
  end
end
```

Then you will install 20.04 ubuntu server. 

The magic goes now.  Just run the next command. If you want to see the creation of your machine, you can open your VBox program

```
vagrant up
```

You will see something like that:

```
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'ubuntu/focal64' could not be found. Attempting to find an
    default: Box Provider: virtualbox
    default: Box Version: >= 0
==> default: Loading metadata for box 'ubuntu/focal64'
    default: URL: https://vagrantcloud.com/ubuntu/focal64
==> default: Adding box 'ubuntu/focal64' (v20210315.0.0) for provider: virt
    default: Downloading: https://vagrantcloud.com/ubuntu/boxes/focal64/ver
Download redirected to host: cloud-images.ubuntu.com
    default:
  https://www.virtualbox.org/manual/ch04.html#sharedfolders
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
==> default: Mounting shared folders...
    default: /vagrant => D:/Users/doneber/Vagrant/Ubuntu-focal64
```

If every was done ok, you can run to connect with the VM
```
vagrant ssh
```

Now you are in the virtual machine created, CONGRATULATIONS!! 

## Connect your VM with SSH (optional)

In your host machine (non-VM) create a ssh key with
(when it ask you for a location, you can use `deploy`)
```
ssh-keygen -t rsa
```

Now enter in your VM with `vagrant ssh` as root

```
sudo su -
```

Copy the public ssh key that was created

```
echo "ssh-rsa AGNWKg6yEzHsrcJUFjsmTZ1Yg7IqQ9GMlA8ncJ9evHIO3/3GxOyGJkJ1FQJyxEckWs3835vgd0rfTPHcg5muMUJjbC37CIY+wChUaFwQupKRaiU2fJv7RI1hLAzWYQwxroRv4HKOf6h2OkmWqvOiAo/6R7rfRbLMEF5V4pbmPxlXJNx3AobOZxlVTAga4xp11j2icZiBOq2izH5hbmsBYpF2kmJYpQG7qSvAcvndfiUkPn+RrsnKH9gpySy+mYwb3dM4r3wvgy15u0xttnTCUzPyANwbp+VJosAo6YfAQV06PF4ufrzflBn3M/2oL9e1SPduCbATMGNuNbH+c2FMAM4IjeVYlu+polb7LmTwQsb7V5ZmaVAqYlTzqQl1Z0= eber.quenta@LabA-09" >  .ssh/authorized_keys
```

and verify the ssh was created with cat .ssh-authorized_keys

Now move to the project and use:
```
ssh root@127.0.0.1 -p 2222 -i ssh/deploy
```

(Where `ssh/deploy` are your ssh keys)

Now you should be connected succesfully via ssh

CONGRATS again! You are on fire! 


## Refs:

- [attaching-new-hard-disk](https://www.vagrantup.com/docs/disks/usage#attaching-new-hard-disks)
- https://github.com/dwyl/learn-vagrant
- (vagrant in windows)[https://www.vagrantup.com/docs/other/wsl]
- [automate-ansible-vagrant](https://www.the-digital-life.com/automate-ansible-vagrant/) Recommended to start